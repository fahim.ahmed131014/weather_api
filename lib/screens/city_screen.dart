import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CityScreen extends StatefulWidget {
  const CityScreen({Key? key}) : super(key: key);

  @override
  _CityScreenState createState() => _CityScreenState();
}

class _CityScreenState extends State<CityScreen> {

  TextEditingController? _textEditingController;


  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/city.jpg"),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.white54.withOpacity(0.4),
                BlendMode.dstATop),
          ),
        ),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: AlignmentDirectional.topStart,
                child: IconButton(
                    onPressed: (){
                       Navigator.pop(context);
                    },
                    icon: Icon(FontAwesomeIcons.backward)),
              ),
              Container(
                padding: EdgeInsets.only(top: 250, left:50, right: 50),
                child: TextField(
                  controller: _textEditingController,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    hintText: "Enter Your City",
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    icon: Icon(FontAwesomeIcons.city),

                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 30.0,left:28),
                child: TextButton(
                    onPressed: (){
                       Navigator.pop(context,_textEditingController!.text);
                    },
                    child: Text("Get Weather",
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                    ),
                ),
              ),

            ],
          ),
        ),
      ),

    );
  }
}
