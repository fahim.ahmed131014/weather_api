import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'package:weather_api/provider/weather_provider.dart';
import 'package:weather_api/screens/city_screen.dart';


class LocationScreen extends StatefulWidget {
  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {





  @override
  void initState() {
    super.initState();
    WeatherProvider weatherProvider = Provider.of<WeatherProvider>(context, listen:false);
    weatherProvider.getCurrentLocation();
  }









  @override
  Widget build(BuildContext context) {
    return Consumer<WeatherProvider>(
      builder: (_,provider,___){
        return Scaffold(
          body: provider.isLoading== false?Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/destination.jpg",),
                  fit: BoxFit.fitHeight,
                  colorFilter: ColorFilter.mode(
                      Colors.white54.withOpacity(0.4),
                      BlendMode.dstATop),
                ),
              ),
              child:SafeArea(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextButton(
                              onPressed: (){
                                provider.isLoading = true;
                                provider.getCurrentLocation();

                              },
                              child: Icon(FontAwesomeIcons.telegram,
                                size: 40,
                                color: Colors.white,),
                            ),
                            TextButton(
                              onPressed: ()async{
                                try{
                                  String cityName = await Navigator.push(context, MaterialPageRoute(builder: (context){
                                    return CityScreen();
                                  }));
                                  provider.isLoading = true;
                                  provider.getWeatherRepositoryByCityName(cityName);
                                } catch(error){
                                  print(error);
                                  provider.isLoading = true;
                                }


                              },
                              child: Icon(FontAwesomeIcons.city,
                                  size: 40,
                                  color: Colors.white),
                            ),

                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left:50.0),
                          child: Text(

                            "${provider.weatherInfo.main!.temp!.toInt()} °",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 80,
                              fontWeight: FontWeight.bold,
                              color: Colors.yellowAccent,
                            ),
                          ),
                        ),
                        Icon(
                          provider.weatherCondition.iconDecider(provider.weatherInfo.weather![0].id!),
                          size: 200,
                        ),
                        Text(
                          "${provider.weatherCondition.weatherStatus(provider.weatherInfo.weather![0].id!)}",
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                          ) ,
                        ),

                        Text(
                          "${provider.weatherInfo.name}",
                          style: const TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: Colors.yellow,

                          ),
                        ),

                      ],
                    ),
                  )
              )
          ):const Center(child: CircularProgressIndicator()),
        );
      },

    );
  }
}
