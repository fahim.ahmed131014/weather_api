import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_api/provider/weather_provider.dart';
import 'package:weather_api/screens/location_screen.dart';

void main() {
  runApp(
    MultiProvider(
        providers: [
          ChangeNotifierProvider<WeatherProvider>(create: (context)=>
              WeatherProvider(),
          ),
        ],
      child: WeatherApi(),
    ),
  );
}

class WeatherApi extends StatelessWidget {
  const WeatherApi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: LocationScreen(),
    );
  }
}


