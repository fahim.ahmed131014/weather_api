import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:weather_api/models/my_location.dart';
import 'package:weather_api/models/weather_condition.dart';
import 'package:weather_api/models/weather_info.dart';
import 'package:weather_api/repository/weather_repository.dart';

class WeatherProvider with ChangeNotifier{
  bool? _isLoading = true;
  WeatherInfo?_weatherInfo;
  WeatherCondition? _weatherCondition = WeatherCondition();

  void getCurrentLocation()async{
    MyLocation myLocation = MyLocation();
    await myLocation.getCurrentLocation();
    double? lat = myLocation.lat;
    double? lon = myLocation.lon;
    getWeatherRepository(lat!, lon!);
  }

  Future<void> getWeatherRepository(double lat, double lon)async{

    try{
      Response response = await WeatherRepository().getWeather(lat,  lon);
      if(response.statusCode == 200){
        _weatherInfo = WeatherInfo.fromJson(response.data);
        isLoading = false;
        notifyListeners();

      }
      else{
        isLoading = false;
        notifyListeners();

      }

    }catch(error)
    {
      print(error);
      isLoading = false;
      notifyListeners();

    }

  }

  Future<void> getWeatherRepositoryByCityName(String cityName)async{

    try{
      Response response = await WeatherRepository().getWeatherByCity(cityName);
      if(response.statusCode == 200){
        _weatherInfo = WeatherInfo.fromJson(response.data);
        isLoading = false;
        notifyListeners();

      }
      else{
        isLoading = false;
        notifyListeners();

      }

    }catch(error)
    {
      print(error);
      isLoading = false;
      notifyListeners();

    }

  }

  WeatherInfo get weatherInfo => _weatherInfo!;


  bool get isLoading => _isLoading!;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  WeatherCondition get weatherCondition => _weatherCondition!;

  set weatherCondition(WeatherCondition value) {
    _weatherCondition = value;
    notifyListeners();
  }
}