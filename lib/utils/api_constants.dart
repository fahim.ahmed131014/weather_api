abstract class ApiConstant{
  static const String BASE_API = "http://api.openweathermap.org/data/2.5/weather";
  static const String APP_ID = "fbd25e534f22659b846f9db87b87b55b" ;
  static const String TEMP_UNIT = "metric" ;
}