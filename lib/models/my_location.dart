import 'package:geolocator/geolocator.dart';

class MyLocation{
  double? lat;
  double? lon;


  Future getCurrentLocation()async{
    try{
      Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      lat = position.latitude;
      lon = position.longitude;
      return position;
    }catch(error){
      print(error);
    }


  }
}