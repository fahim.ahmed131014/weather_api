import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WeatherCondition{

  IconData iconDecider(int id)
  {
    if(id>=200 && id<300)
      {
        return FontAwesomeIcons.cloudRain;
      }
    else if(id>=300 && id<600)
      {
        return FontAwesomeIcons.cloud;
      }
    else if(id>=600 && id<800)
      {
        return FontAwesomeIcons.snowboarding;
      }
    else if(id == 800)
      {
        return FontAwesomeIcons.sun;
      }
    else{
      return FontAwesomeIcons.cloudSun;
    }
  }
  String weatherStatus(int id)
  {
    if(id>=200 && id<300)
    {
      return "Cloudy, best day to listen some soft tunes";
    }
    else if(id>=300 && id<600)
    {
      return "Rain will come at any time, better have an umbrella";
    }
    else if(id>=600 && id<800)
    {
      return "Snowy, put some jacket";
    }
    else if(id == 800)
    {
      return "Perfectly sunny day, good day for a sunbath";
    }
    else{
      return "Rainy,Take an umbrella" ;
    }
  }



}