import 'package:dio/dio.dart';
import 'package:weather_api/utils/api_constants.dart';
import 'package:weather_api/utils/constants.dart';

class WeatherRepository{

  Duration duration = Duration(seconds: 60);

  Future<Response> getWeather(double? lat , double? lon)async{

    String api = "${ApiConstant.BASE_API}?APPID=${ApiConstant.APP_ID}&lat=$lat&lon=$lon&units=${ApiConstant.TEMP_UNIT}";


    Response response = await Dio().get(
       api,
    ).
    timeout(duration);
    return response;

  }


  Future<Response> getWeatherByCity(String cityName)async{

    String api = "${ApiConstant.BASE_API}?APPID=${ApiConstant.APP_ID}&q=$cityName&units=${ApiConstant.TEMP_UNIT}";


    Response response = await Dio().get(
      api,
    ).
    timeout(duration);
    return response;

  }
}